import allure
from playwright.sync_api import Page, expect

from pages.file_upload_page import FileUploadPage


class FileUploadPageStep:
    def __init__(self, page: Page) -> None:
        self.file_upload_page = FileUploadPage(page)

    @allure.step("Check file uploading")
    def check_file_upload_work(self) -> None:
        self.file_upload_page.upload_input().set_input_files("data/test.csv")
        self.file_upload_page.upload_button().click()
        expect(self.file_upload_page.file_uploaded_label()).to_be_visible()
