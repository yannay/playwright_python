import allure
from playwright.sync_api import Page, expect

from pages.auth_page import AuthPage
from pages.file_upload_page import FileUploadPage
from pages.hovers_page import HoversPage
from pages.main_page import MainPage


class MainPageStep:
    def __init__(self, page: Page) -> None:
        self.main_page = MainPage(page)
        self.auth_page = AuthPage(page)
        self.file_upload_page = FileUploadPage(page)
        self.hovers_page = HoversPage(page)

    @allure.step("Redirect to the MainPage")
    def goto(self) -> None:
        self.main_page.goto()

        # Expect a title "to contain" a substring.
        expect(self.main_page.page).to_have_title("The Internet")

    @allure.step("Click on the Basic Auth link")
    def click_on_the_basic_auth_link(self) -> None:
        self.main_page.auth_link().click()
        expect(self.auth_page.basic_auth_label()).to_be_visible()

    @allure.step("Click on the File Upload link")
    def click_on_the_file_upload_link(self) -> None:
        self.main_page.file_upload_link().click()
        expect(self.file_upload_page.file_uploader_label()).to_be_visible()

    @allure.step("Click on the Hovers link")
    def click_on_the_hovers_link(self) -> None:
        self.main_page.hovers_link().click()
        expect(self.hovers_page.hovers_label()).to_be_visible()
