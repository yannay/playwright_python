import allure
from playwright.sync_api import Page, expect

from pages.hovers_page import HoversPage


class HoversPageStep:
    def __init__(self, page: Page) -> None:
        self.hovers_page = HoversPage(page)

    @allure.step("Hover over image and check username")
    def check_hovers_work(self) -> None:
        expect(self.hovers_page.first_user_name()).not_to_be_visible()
        self.hovers_page.first_avatar_image().hover()
        expect(self.hovers_page.first_user_name()).to_be_visible()
