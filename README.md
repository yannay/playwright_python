## **Установка проекта и зависимостей:**
* docker build -t playwright-python .

## **Запуск тестов**
* docker run -it -v $(pwd)/results:/usr/src/app/results playwright-python

## **Сборка окружения + Запуск тестов + Открыть отчет**
* docker compose build
* docker compose up
* в новом терминале:
* docker exec -it allure-commandline allure serve results --port 8000