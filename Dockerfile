FROM python:3.9

ENV APP_PATH=/usr/src/app
WORKDIR $APP_PATH

# установка нужного часового пояса
ENV TZ=Europe/Moscow
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN date

# install by pip
# COPY requirements.txt ./
# RUN pip install --no-cache-dir -r requirements.txt

# install by poetry
RUN curl -sSL https://install.python-poetry.org | POETRY_HOME=/etc/poetry POETRY_CACHE_DIR=/etc/poetry/.cache/pypoetry python3 -
ENV PATH="$PATH:/etc/poetry/bin"
RUN poetry --version
COPY pyproject.toml poetry.lock ./
# RUN poetry config cache-dir /etc/poetry/.cache/pypoetry
RUN poetry config virtualenvs.create false --local && poetry install --no-dev
RUN chmod +r poetry.toml

# install playwright env
RUN echo "deb http://ftp.us.debian.org/debian buster main non-free" >> /etc/apt/sources.list.d/fonts.list
RUN PLAYWRIGHT_BROWSERS_PATH=$APP_PATH/pw-browsers poetry run playwright install chromium
RUN poetry run playwright install-deps chromium

COPY . ./

EXPOSE 8000
CMD ["poetry", "run", "pytest", "-v", "-s", "-m", "herokuapp", "--alluredir", "results"]
#ENTRYPOINT ["/bin/bash"]
