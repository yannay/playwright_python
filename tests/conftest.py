import allure
import pytest
from playwright.sync_api import Browser, Page

from config import password, username


@pytest.fixture()
def page_with_auth(browser: Browser) -> Page:
    context = browser.new_context(
        http_credentials={"username": username, "password": password}, record_video_dir="videos/"
    )
    page = context.new_page()
    yield page
    context.close()
    allure.attach(
        open(page.video.path(), "rb").read(), name="Video attach", attachment_type=allure.attachment_type.WEBM
    )


@pytest.fixture()
def page(browser: Browser) -> Page:
    context = browser.new_context(record_video_dir="videos/")
    page = context.new_page()
    yield page
    context.close()
    allure.attach(
        open(page.video.path(), "rb").read(), name="Video attach", attachment_type=allure.attachment_type.WEBM
    )
