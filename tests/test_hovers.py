import allure
import pytest
from playwright.sync_api import Page

from config import url
from steps.hovers_page_step import HoversPageStep
from steps.main_page_step import MainPageStep


@pytest.mark.herokuapp
@allure.suite("herokuapp")
@allure.title("Test with hover")
@allure.description("Test with hover")
@allure.link(f"{url}/hovers")
@allure.severity(allure.severity_level.NORMAL)
def test_hovers(page: Page) -> None:

    # redirect to the MainPage
    main_page_step = MainPageStep(page)
    main_page_step.goto()

    # click on the hovers link
    main_page_step.click_on_the_hovers_link()

    # hover over image and check username
    hovers_page_step = HoversPageStep(page)
    hovers_page_step.check_hovers_work()
