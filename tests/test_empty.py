import allure
import pytest


@pytest.mark.empty
@allure.suite("empty")
@allure.title("Skipped test")
@allure.description("Test which we will skip")
@allure.severity(allure.severity_level.TRIVIAL)
@pytest.mark.skip(reason="This test is empty")
def test_skipped() -> None:
    pass


@pytest.mark.empty
@allure.suite("empty")
@allure.title("Xfailed test")
@allure.description("Test which will be failed")
@allure.severity(allure.severity_level.TRIVIAL)
@pytest.mark.xfail(reason="Wrong operation")
def test_xfailed() -> None:
    assert 1 == 2  # type: ignore


@pytest.mark.empty
@allure.suite("empty")
# @allure.title(f"Parametrized test")
# @allure.description("Test with parametr")
@allure.severity(allure.severity_level.TRIVIAL)
@pytest.mark.parametrize("param_my", [1, 2], ids=["First parametrized test", "Second parametrized test"])
def test_parametrized(param_my: int) -> None:
    # allure.title(f"Parametrized test for param")
    allure.dynamic.description(f"Test with parametr == {param_my}")
    allure.dynamic.title(f"Parametrized test for param = {param_my}")
    assert param_my < 3
