import allure
import pytest
from playwright.sync_api import Page

from config import url
from steps.main_page_step import MainPageStep


@pytest.mark.herokuapp
@allure.suite("herokuapp")
@allure.title("Test on Basic Auth")
@allure.description("Test on Basic Auth")
@allure.link(f"{url}/basic_auth")
@allure.severity(allure.severity_level.CRITICAL)
def test_auth(page_with_auth: Page) -> None:

    # redirect to the MainPage
    main_page_step = MainPageStep(page_with_auth)
    main_page_step.goto()

    # click on the basic auth link
    main_page_step.click_on_the_basic_auth_link()

    # page_with_auth.wait_for_timeout(5000)
