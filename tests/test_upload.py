import allure
import pytest
from playwright.sync_api import Page

from config import url
from steps.file_upload_page_step import FileUploadPageStep
from steps.main_page_step import MainPageStep


@pytest.mark.herokuapp
@allure.suite("herokuapp")
@allure.title("Test on file uploading")
@allure.description("Test on file uploading")
@allure.link(f"{url}/upload")
@allure.severity(allure.severity_level.NORMAL)
def test_upload(page: Page) -> None:

    # redirect to the MainPage
    main_page_step = MainPageStep(page)
    main_page_step.goto()

    # click on the file upload link
    main_page_step.click_on_the_file_upload_link()

    # check file uploading
    file_upload_page_step = FileUploadPageStep(page)
    file_upload_page_step.check_file_upload_work()
