from playwright.sync_api import Locator, Page

from config import url


class MainPage:
    def __init__(self, page: Page) -> None:
        self.page = page

    def goto(self) -> None:
        self.page.goto(url)

        # # Expect a title "to contain" a substring.
        # expect(self.page).to_have_title("The Internet")

    def auth_link(self) -> Locator:
        return self.page.get_by_role("link", name="Basic Auth")

    def file_upload_link(self) -> Locator:
        return self.page.get_by_role("link", name="File Upload")

    def hovers_link(self) -> Locator:
        return self.page.get_by_role("link", name="Hovers")
