from playwright.sync_api import Locator, Page


class HoversPage:
    def __init__(self, page: Page) -> None:
        self.page = page

    def hovers_label(self) -> Locator:
        return self.page.get_by_role("heading", name="Hovers")

    def first_avatar_image(self) -> Locator:
        return self.page.get_by_alt_text("User Avatar").first

    def first_user_name(self) -> Locator:
        return self.page.get_by_role("heading", name="name: user1")
