from playwright.sync_api import Locator, Page  # , expect


class AuthPage:
    def __init__(self, page: Page) -> None:
        self.page = page

    def basic_auth_label(self) -> Locator:
        return self.page.get_by_role("heading", name="Basic Auth")
