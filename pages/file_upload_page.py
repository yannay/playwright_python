from playwright.sync_api import Locator, Page


class FileUploadPage:
    def __init__(self, page: Page) -> None:
        self.page = page

    def file_uploader_label(self) -> Locator:
        return self.page.get_by_role("heading", name="File Uploader")

    def upload_input(self) -> Locator:
        return self.page.locator("id=file-upload")

    def upload_button(self) -> Locator:
        return self.page.locator("id=file-submit")

    def file_uploaded_label(self) -> Locator:
        return self.page.get_by_role("heading", name="File Uploaded!")
